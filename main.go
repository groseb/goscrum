package main

import (
	"os"
	"runtime"

	"github.com/codegangsta/cli"

	"dev.groseb.net/groseb/GoScrum/cmd"
	"dev.groseb.net/groseb/GoScrum/modules/setting"
)

const APP_VER = "0.0.1"

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	setting.AppVer = APP_VER
}

func main() {
	//setting.NewConfigContext()
	app := cli.NewApp()
	app.Name = "GameServersManager"
	app.Usage = "Go GSM Service"
	app.Version = APP_VER
	app.Commands = []cli.Command{
		//cmd.CmdUpdate,
		//cmd.CmdDump,
		cmd.CmdAPI,
		cmd.CmdWeb,
		cmd.CmdInstall,
	}
	app.Flags = append(app.Flags, []cli.Flag{}...)
	app.Run(os.Args)
}