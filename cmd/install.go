package cmd

import (
	_ "github.com/mattn/go-sqlite3"
	"github.com/jinzhu/gorm"
	//"fmt"
	"log"

	"github.com/codegangsta/cli"
	"dev.groseb.net/groseb/GoScrum/models"
)

var CmdInstall = cli.Command{
	Name:  "install",
	Usage: "Install GoScrum",
	Description: `Initialize GoScrum for the first time`,
	Action: runInstall,
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "port, p",
			Value: "3000",
			Usage: "Temporary port number to prevent conflict",
		},
	},
}

func runInstall(ctx *cli.Context) {
	// this Pings the database trying to connect, panics on error
	// use sqlx.Open() for sql.Open() semantics
	//db, err := sqlx.Connect("mysql", "root:azerty@tcp(192.168.56.20:3306)/scrum")
	db, err := gorm.Open("sqlite3", "./database.db")
	if err != nil {
		log.Fatalln(err)
	}
	// Get database connection handle [*sql.DB](http://golang.org/pkg/database/sql/#DB)
	db.DB()

	// Then you could invoke `*sql.DB`'s functions with it
	db.DB().Ping()

	db.CreateTable(&models.Comment{})
	db.CreateTable(&models.Item{})
	db.CreateTable(&models.List{})
	db.CreateTable(&models.Project{})
	db.CreateTable(&models.Sprint{})
	db.CreateTable(&models.Team{})
	db.CreateTable(&models.User{})
}