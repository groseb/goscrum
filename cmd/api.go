package cmd

import (
	"github.com/codegangsta/cli"
	"github.com/codegangsta/negroni"
	"log"
	"net/http"

	"dev.groseb.net/groseb/GoScrum/modules/authentication"
	"dev.groseb.net/groseb/GoScrum/modules/router"
)

var CmdAPI = cli.Command{
	Name:  "api",
	Usage: "Start GoScrum API server",
	Description: `GoScrum API server can be run if you want to use an external web hosting`,
	Action: runAPI,
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "port, p",
			Value: "3000",
			Usage: "Temporary port number to prevent conflict",
		},
	},
}

func runAPI(ctx *cli.Context) {
	router := router.NewRouter()
	port := ctx.String("port")

	n := negroni.New()
	// Authentication
	n.Use(negroni.HandlerFunc(authentication.RequireTokenAuthentication))
	// router goes last
	n.UseHandler(router)

	log.Fatal(http.ListenAndServe(":" + port, n))
}