package cmd

import (
	"github.com/codegangsta/cli"
)

type Page struct {
	Title string
}

var CmdWeb = cli.Command{
	Name:  "web",
	Usage: "Start GoScrum web server",
	Description: `GoScrum web server is the only thing you need to run,
and it takes care of all the other things for you`,
	Action: runWeb,
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:  "port, p",
			Value: "3000",
			Usage: "Temporary port number to prevent conflict",
		},
	},
}

func runWeb(ctx *cli.Context) {

}