package models

import "time"

type Sprint struct {
	ID		uint64
	Lists	[]List
	Items	[]Item
	Start 	time.Time
	End		time.Time
}