package models

type Comment struct {
	ID		uint64
	Data	string
	Author	User
}