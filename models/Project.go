package models

type Project struct {
	ID		uint64
	Sprints	[]Sprint
}