package models

type Item struct  {
	ID			uint64
	Name		string
	Comments	[]Comment
}