package models

type List struct  {
	ID		uint64
	Items	[]Item
}