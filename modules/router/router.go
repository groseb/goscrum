package router

import (
	"net/http"

	"dev.groseb.net/groseb/GoScrum/modules/logger"
	"dev.groseb.net/groseb/GoScrum/modules/routes"
	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes.RoutesList {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = logger.Logger(handler, route.Name)

		router.
		Methods(route.Method).
		Path(route.Pattern).
		Name(route.Name).
		Handler(handler)

	}
	return router
}