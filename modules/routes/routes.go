package routes

import (
	"net/http"

	"dev.groseb.net/groseb/GoScrum/modules/handlers"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var RoutesList = Routes{
	Route{
		"Index",
		"GET",
		"/",
		handlers.Index,
	},
	Route{
		"TodoIndex",
		"GET",
		"/items",
		handlers.ItemsIndex,
	},
	Route{
		"TodoShow",
		"GET",
		"/item/{itemId}",
		handlers.ItemShow,
	},
	Route{
		"Login",
		"POST",
		"/login",
		handlers.Login,
	},
	Route{
		"Logout",
		"GET",
		"/logout",
		handlers.Logout,
	},
	Route{
		"Refresh",
		"GET",
		"/refresh",
		handlers.RefreshToken,
	},
}