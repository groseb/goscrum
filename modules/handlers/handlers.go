package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"dev.groseb.net/groseb/GoScrum/models"
	"github.com/gorilla/mux"
	"dev.groseb.net/groseb/GoScrum/services"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome!")
}

func ItemsIndex(w http.ResponseWriter, r *http.Request) {
	items := []models.Item{}
	comments := []models.Comment{}
	comments = append(comments, models.Comment{ID: 0, Author: models.User{ID:0, Username: "seb"}, Data: "le commentaire"})
	items = append(items, models.Item{ID: 0, Comments: comments, Name: "Write presentation"})
	items = append(items, models.Item{ID: 1, Comments: nil, Name: "Host meetup"})

	if err := json.NewEncoder(w).Encode(items); err != nil {
		panic(err)
	}
}

func ItemShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	itemId := vars["itemId"]
	fmt.Fprintln(w, "Item show:", itemId)
}

func Login(w http.ResponseWriter, r *http.Request) {
	requestUser := new(models.User)
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&requestUser)

	responseStatus, token := services.Login(requestUser)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseStatus)
	w.Write(token)
}

func RefreshToken(w http.ResponseWriter, r *http.Request) {
	requestUser := new(models.User)
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&requestUser)

	w.Header().Set("Content-Type", "application/json")
	w.Write(services.RefreshToken(requestUser))
}

func Logout(w http.ResponseWriter, r *http.Request) {
	err := services.Logout(r)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}