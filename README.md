GoScrum
=======

Go software to manage agile projects

Only an API at the moment, this is a work in progress

-------

Required packages :
--------------------

- `go install github.com/mattn/go-sqlite3`
- `go install github.com/jinzhu/gorm`
- `go install github.com/codegangsta/cli`
- `go install github.com/dgrijalva/jwt-go`
- `go install github.com/codegangsta/negroni`
- `go install golang.org/x/crypto/bcrypt`

Use `go get ./..` to get all packages

Use `-a` to force update

Usage :
-------

### Login :

Query :

`curl -H "Content-Type: application/json" -X POST -d '{"username": "haku","password": "testing"}' http://localhost:3000/login`

Response:

```json
{"token":"$access_token"}
```

### Token usage :

Query (using header):
`curl -X GET -H "Authorization: Bearer $access_token" "http://localhost:3000/items"`

Query (using get parameter):
`curl -X GET "http://localhost:3000/items?access_token=$access_token"`

Response:
```json
[{"ID":0,"Name":"card name","Comments":[{"ID":0,"Data":"comment","Author":{"uuid":0,"username":"seb","password":""}}]},{"ID":1,"Name":"card name","Comments":null}]
```